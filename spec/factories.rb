FactoryGirl.define do
  factory :user do
    name     "Kenny"
    password "999999"
    password_confirmation "999999"
  end
  factory :message do
    text 'hello'
    user
  end
end
