require "rails_helper"
RSpec.describe Message, :type => :model do
  context "validates" do
    let!(:message_test) { FactoryGirl.create(:message) }
    it "is valid with valid attributes" do
      expect(message_test).to be_valid
    end
    it "is not valid without user_id" do
      message_test.user_id = nil 
      expect(message_test).not_to be_valid
    end
    it "is not valid without a text" do
      message_test.text = nil
      expect(message_test).not_to be_valid
    end
  end
end