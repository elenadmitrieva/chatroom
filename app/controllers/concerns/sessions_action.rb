module SessionsAction
extend ActiveSupport::Concern

  def sign_in(user)
    remember_token = User.new_remember_token
    cookies.permanent[:remember_token] = remember_token
    user.update_attribute(:remember_token, User.encrypt(remember_token))
    self.current_user = user
  end
  def current_user=(user)
    @current_user = user
  end
  def sign_out
    current_user.update_attribute(:remember_token, User.encrypt(User.new_remember_token))
    cookies.delete(:remember_token)
    self.current_user = nil
  end
  def signed_in_user
      redirect_to signin_path, notice: "If you want to use the chat, please sign in." unless signed_in?
  end
end
