class UsersController < ApplicationController
  before_action :signed_in_user, only: :index
 
  def index
    @users = User.all
  end

  def new
  	@user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      sign_in @user
    	redirect_to messages_path
    else
      render 'new'
    end
  end

  private

    def user_params
      params.require(:user).permit(:name, :password,:password_confirmation)
    end
end
