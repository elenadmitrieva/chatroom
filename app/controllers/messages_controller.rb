class MessagesController < ApplicationController
  before_action :signed_in_user

  def index
    @messages = Message.last(5)
  end

  def new_messages
    if params[:id].to_i != 0
      @new_messages = Message.where("id > ?", params[:id].to_i)
    else
      redirect_to messages_path
    end
  end

  def create
    @message = Message.new(message_params)
    @message.user = current_user
    @message.save
  end

  private
    def message_params
    	params.require(:message).permit(:text)
    end
 
end
 